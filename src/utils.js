export const decreasePhoneInCart = (phone, phonesInCart) => {
    phonesInCart.some(phoneInCart => {
        if (phoneInCart.id === phone.id) {
            if (phoneInCart.quantity > 1) {
                phoneInCart.quantity -= 1
                phoneInCart.quantityPrice -= phone.price
            }
            else {
                let index = phonesInCart.indexOf(phoneInCart)
                phonesInCart.slice(index, index+1)
            }
            return true;
        }
    });
    return phonesInCart;
}