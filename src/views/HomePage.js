import React from "react";
import Cart from "../components/Cart";
import Total from "../components/Total";
import Prime from "../components/Prime";
import {phonesList} from "../components/phonesList";
import Phones from "../components/Phones";
import "./HomePage.css";
import { decreasePhoneInCart } from "../utils";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { phonesInCart: [], renderNextPage: true, renderPrime: false};
  }
  addPhoneToCart = phone => {
    if (!this.state.phonesInCart.includes(phone)) {
      phone.quantity=1
      phone.quantityPrice=phone.price
      this.setState({ phonesInCart: this.state.phonesInCart.concat([phone])});
    }
    else{
        this.increasePhoneQuantity(phone)
    }
  };

  increasePhoneQuantity = phone => {
    this.state.phonesInCart.some(phoneInCart => {
        if (phoneInCart.id === phone.id) {
            phoneInCart.quantity += 1
            phoneInCart.quantityPrice += phone.price

        }
    });
    this.setState({phonesInCart: this.state.phonesInCart})
  }

  decreasePhoneQuantity = phone => {
//    this.state.phonesInCart.some(phoneInCart => {
//        if (phoneInCart.id === phone.id) {
//            if (phoneInCart.quantity > 1) {
//            phoneInCart.quantity -= 1
//            phoneInCart.quantityPrice -= phone.price
//            }
//            else {
//                this.state.phonesInCart = this.state.phonesInCart.filter(phone => phoneInCart.id !== phone.id);
//            }
//        }
//    });
    let newPhonesInCart = decreasePhoneInCart(phone, this.state.phonesInCart);
    this.setState({phonesInCart: newPhonesInCart})
  }

  updatePhonesInCart(value) {
    this.setState({phonesInCart: value});
  }

  renderNextPage = () => {
    this.setState({renderNextPage:!this.state.renderNextPage})
  }
  renderPrime=()=>{
      this.renderNextPage()
      this.setState({renderPrime:true})
  }
  phones = () => {
    return <div className="divHome">
    <div className="Heading"><h1>Mobile Cart</h1></div>
    <div className="noOfPhonesInCart"><p>Cart: {this.state.phonesInCart.length}</p></div>
    <div className="phones">
          <Phones phones={phonesList} addPhoneToCart={this.addPhoneToCart}/>
    </div>
    <div className="cart">
        <button onClick={this.renderNextPage}>Go to cart</button>
        <button onClick={this.renderPrime}>prime Offers</button>
        </div>
    </div> 
  }

  nextPage = () => {
    const cart = <Cart phonesInCart={this.state.phonesInCart} increaseQuantity={this.addPhoneToCart} decreaseQuantity={this.decreasePhoneQuantity} goBack={this.renderNextPage}/>
    if(!this.state.renderPrime) {
    return <div>
                {cart}
                <Total phonesInCart={this.state.phonesInCart}/>
            </div>
    }
    else{
        return <div>
            {cart}
            <h1>Be a Prime member and get 40% offer on your cart total!!!!!!</h1>
            <Total phonesInCart={this.state.phonesInCart}/>
            <Prime phonesInCart={this.state.phonesInCart}/>
        </div>
    }
  }

  render() {
    return this.state.renderNextPage ? <this.phones /> : <this.nextPage/>;
  }
}

export default HomePage;
