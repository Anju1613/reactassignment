import React from 'react';
import './css/App.css';
import HomePage from './views/HomePage.js';

function App() {
  return (
    <div className="App">
      <HomePage />
    </div>
  );
}

export default App;
