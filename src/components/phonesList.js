export const phonesList = [{
    id: 1,
    imageUrl: "../Images/Honor 8C.jpg",
    name: "Honor 8C (Blue, 4GB RAM, 32GB Storage)",
    price: 100
},
{
    id: 2,
    imageUrl: "../Images/Honor9N.jpg",
    name: "Honor 9N (Blue, 4GB RAM, 64GB Storage)",
    price: 200
},
{
    id: 3,
    imageUrl: "../Images/Nokia6.jpg",
    name: "Nokia 6.1 Plus (Blue, 6GB RAM, 64GB Storage)",
    price: 300  
},
{
    id: 4,
    imageUrl:"../Images/OppoA7.jpg",
    name:"OPPO A7 (Glaze Blue, 3GB RAM, 64GB Storage)",
    price: 400
},
{
    id: 5,
    imageUrl: "../Images/OppoK3.jpg",
    name: "OPPO K3 (Aurora Blue, 8GB RAM, 128GB Storage)",
    price: 500
},
{
    id: 6,
    imageUrl: "../Images/Redmi7.jpg",
    name: "Redmi 7 (Comet Blue, 3GB RAM, 32GB Storage)",
    price: 600
}]