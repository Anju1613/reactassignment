import React from 'react';
import Phone from "./Phone.js";
import './css/Phones.css';
import {phonesList} from "./phonesList.js";

class Phones extends React.Component {
    
    render() {
        return (
            <div className="phones">
            {phonesList.map(phone=> {
                return <Phone key={phonesList.id} phone={phone} onClicked={this.props.addPhoneToCart} />
              })}
            </div>
        );
    }
}

export default Phones;