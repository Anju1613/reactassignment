import React from 'react';
import "./css/Prime.css";

function Prime(props){
    const total = props.phonesInCart.reduce(function(total,phone){return total + phone.quantityPrice},0)
    const primeOffer = total * 0.4
    return (
            <div className="prime">
                  PrimeOffer: {primeOffer}
            </div>
    );
}

export default Prime;