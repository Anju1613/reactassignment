import React from 'react';
import './css/Total.css';

function Total(props) {
    const total = props.phonesInCart.reduce((total,phone) => total + phone.quantityPrice, 0)

    return(
        <div className="cartTotal">CartTotal: {total}</div>
    )
}

export default Total