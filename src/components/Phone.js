import React from 'react';
import './css/Phone.css';
import "../Images/OppoK3.jpg";

function Phone(props) {
//    console.log(props.phone.imageUrl);

    return (
        <div>
            <div className="container">
               <div className="image"> <img src={require("/Users/skhandavalli/Desktop/React/assignment/src/Images/Honor 8C.jpg")} alt={props.phone.name}/> </div>
               <div className="name"><p>{props.phone.name}</p></div>
               <div className="price"><p>Price: {props.phone.price} Rs</p></div>
               <div className="addToCart"><button type="button" onClick={()=>props.onClicked(props.phone)}>Add to Cart!</button></div>
            </div>
        </div>
    );
}
export default Phone;