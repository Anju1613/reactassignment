import React from 'react';
import './css/Cart.css';
import Prime from './Prime.js';

class Cart extends React.Component{
    constructor(props){
        super(props)
        this.state = {total:0,renderPrime:false}
    }

    render(){
        return (
            <div>
            <div className="goBack" onClick={this.props.goBack}><button >Go back</button></div>
            <p>Cart: {this.props.phonesInCart.length}</p>
            <div className="phonesInCart">   
                <ul>{this.props.phonesInCart.map((phone) => <li><b> Name: </b>{phone.name} 
                                                    <b> Price: </b>{phone.price}
                                                    <b> Quantity: </b>{phone.quantity}  
                                                    <button type="button" onClick={()=>this.props.decreaseQuantity(phone)}> -1 </button>
                                                    <button type="button" onClick={()=>this.props.increaseQuantity(phone)}> +1 </button>
                                                    <b> TotalPrice: </b>{phone.quantityPrice}</li>)}</ul>
            </div>
            </div>
        );
    }
}

export default Cart;